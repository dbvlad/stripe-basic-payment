// A reference to Stripe.js
var stripe;

var orderData = {
    items: [{ id: "photo-subscription" }],
    currency: "usd"
};

// Disable the button until we have Stripe set up on the page
document.querySelector("button").disabled = true;

// Show a spinner whenever we have server side activity
var changeLoadingState = function (isLoading) {
    if (isLoading) {
        document.querySelector("button").disabled = true;
        document.querySelector("#spinner").classList.remove("hidden");
        document.querySelector("#button-text").classList.add("hidden");
        document.querySelector("#order-amount").classList.add("hidden");
    } else {
        document.querySelector("button").disabled = false;
        document.querySelector("#spinner").classList.add("hidden");
        document.querySelector("#button-text").classList.remove("hidden");
        document.querySelector("#order-amount").classList.remove("hidden");
    }
};

var showOrderResult = function(paymentIntent) {
    var paymentIntentJson = JSON.stringify(paymentIntent, null, 2);
    document.querySelector("pre").textContent = paymentIntentJson;

    document.querySelector(".sr-payment-form").classList.add("hidden");
    
    document.querySelector("#new-payment").classList.remove("hidden");
    document.querySelector(".sr-result").classList.remove("hidden");
    setTimeout(function () {
        document.querySelector(".sr-result").classList.add("expand");
    }, 200);
}

var hideOrderResult = function() {
    document.querySelector(".sr-payment-form").classList.remove("hidden");
    
    document.querySelector("#new-payment").classList.add("hidden");
    document.querySelector(".sr-result").classList.add("hidden");
    document.querySelector(".sr-result").classList.remove("expand");
}

var setup = function () {
    changeLoadingState(true);

    fetch("/create-payment-intent", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(orderData)
    })
        .then(function (result) {
            return result.json();
        })
        .then(function (data) {
            return setupElements(data);
        })
        .then(function ({ stripe, card, clientSecret }) {
            hideOrderResult();
            changeLoadingState(false);

            // Handle form submission.
            var form = document.getElementById("payment-form");
            form.addEventListener("submit", function (event) {
                event.preventDefault();
                // Initiate payment when the submit button is clicked
                pay(stripe, card, clientSecret);
            });
        });
}

setup();

// Set up Stripe.js and Elements to use in checkout form
var setupElements = function (data) {
    stripe = Stripe(data.publishableKey);

    var elements = stripe.elements();
    var style = {
        base: {
            color: "#32325d",
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: "antialiased",
            fontSize: "16px",
            "::placeholder": {
                color: "#aab7c4"
            }
        },
        invalid: {
            color: "#fa755a",
            iconColor: "#fa755a"
        }
    };

    var card = elements.create("card", { style: style });
    card.mount("#card-element");

    document.querySelector("#order-amount").textContent = " " + (data.amount / 100.0) + " " + data.currency.toUpperCase();

    document.querySelector("#new-payment").addEventListener("click", function () {
        setup();
    }, {once : true});

    return {
        stripe: stripe,
        card: card,
        clientSecret: data.clientSecret
    };
};

var setErrorMessage = function (errorMsgText) {
    var errorMsg = document.querySelector(".sr-field-error");
    errorMsg.textContent = errorMsgText;
}

/*
 * Calls stripe.confirmCardPayment which creates a pop-up modal to
 * prompt the user to enter extra authentication details without leaving your page
 */
var pay = function (stripe, card, clientSecret) {
    setErrorMessage("");

    changeLoadingState(true);

    // Initiate the payment.
    // If authentication is required, confirmCardPayment will automatically display a modal
    stripe
        .confirmCardPayment(clientSecret, {
            payment_method: {
                card: card
            }
        })
        .then(function (result) {
            if (result.error) {
                // Show error to your customer
                showError(result.error.message);
            } else {
                // The payment has been processed!
                orderComplete(clientSecret);
            }
        });
};

/* ------- Post-payment helpers ------- */

/* Shows a success / error message when the payment is complete */
var orderComplete = function (clientSecret) {
    // Just for the purpose of the sample, show the PaymentIntent response object
    stripe.retrievePaymentIntent(clientSecret).then(function (result) {
        showOrderResult(result.paymentIntent);

        changeLoadingState(false);
    });
};

var showError = function (errorMsgText) {
    changeLoadingState(false);

    setErrorMessage(errorMsgText);
};