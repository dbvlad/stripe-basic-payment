#! /usr/bin/env python3.6

"""
server.py
Stripe Sample.
Python 3.6 or newer required.
"""

import stripe
import json
import os
import uuid
import datetime

from flask import Flask, render_template, jsonify, request, send_from_directory
from dotenv import load_dotenv, find_dotenv

# Setup Stripe python client library
load_dotenv(find_dotenv())
stripe.api_key = os.getenv('STRIPE_SECRET_KEY')
stripe.api_version = os.getenv('STRIPE_API_VERSION')

static_dir = str(os.path.abspath(os.path.join(__file__ , "..", os.getenv("STATIC_DIR"))))

app = Flask(__name__, static_folder=static_dir,
            static_url_path="", template_folder=static_dir)

with open('orders.txt', 'w') as orders_file:
    orders_file.write('Creation date\tOrder id\tOrder details\n')

with open('confirmed-orders.txt', 'w') as confirmed_orders_file:
    confirmed_orders_file.write('Confirmation date\tOrder id\n')


@app.route('/checkout', methods=['GET'])
def get_checkout_page():
    # Display checkout page
    return render_template('index.html')


def calculate_order_amount(items):
    # Replace this constant with a calculation of the order's amount
    # Calculate the order total on the server to prevent
    # people from directly manipulating the amount on the client
    return 1400


@app.route('/create-payment-intent', methods=['POST'])
def create_payment():
    order_id = uuid.uuid4()

    data = json.loads(request.data)

    with open('orders.txt', 'a') as orders_file:
        orders_file.write(f'{datetime.datetime.utcnow()}\t{order_id}\t{json.dumps(data)}\n')
    
    # Create a PaymentIntent with the order amount and currency
    intent = stripe.PaymentIntent.create(
        amount=calculate_order_amount(data['items']),
        currency=data['currency'],
        metadata=dict(
            integration_check='accept_a_payment',
            order_id=order_id
        )
    )

    try:
        # Send publishable key and PaymentIntent details to client
        return jsonify({
            'publishableKey': os.getenv('STRIPE_PUBLISHABLE_KEY'), 
            'clientSecret': intent.client_secret,
            'amount': intent.amount,
            'currency': intent.currency
        })
    except Exception as e:
        return jsonify(error=str(e)), 403


@app.route('/webhook', methods=['POST'])
def webhook_received():
    # You can use webhooks to receive information about asynchronous payment events.
    # For more about our webhook events check out https://stripe.com/docs/webhooks.
    webhook_secret = os.getenv('STRIPE_WEBHOOK_SECRET')
    request_data = json.loads(request.data)

    if webhook_secret:
        # Retrieve the event by verifying the signature using the raw body and secret if webhook signing is configured.
        signature = request.headers.get('stripe-signature')
        try:
            event = stripe.Webhook.construct_event(
                payload=request.data, sig_header=signature, secret=webhook_secret)
            data = event['data']
        except Exception as e:
            return e
        # Get the type of webhook event sent - used to check the status of PaymentIntents.
        event_type = event['type']
    else:
        data = request_data['data']
        event_type = request_data['type']

    data_object = data['object']

    if event_type == 'payment_intent.succeeded':
        order_id = data_object['metadata']['order_id']

        with open('confirmed-orders.txt', 'a') as confirmed_orders_file:
            confirmed_orders_file.write(f'{datetime.datetime.utcnow()}\t{order_id}\n')

        print('💰 Payment received!')
        # Fulfill any orders, e-mail receipts, etc
        # To cancel the payment you will need to issue a Refund (https://stripe.com/docs/api/refunds)
    elif event_type == 'payment_intent.payment_failed':
        print('❌ Payment failed.')
    return jsonify({'status': 'success'})


if __name__ == '__main__':
    app.run()