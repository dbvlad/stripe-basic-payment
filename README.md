# What is this?

Solving for [this homework](https://docs.google.com/document/d/16SgZH2lFcmh1U0ABcdMlkfvupGe2MgxUMDy31qhCVY0/edit).
The write-up can be found [here](https://docs.google.com/document/d/1e05O3Adxo81j8g46Epg1QIwImJWJ4EXkDggn4QuV9Xs/edit).

# Running the code

## Requirements

- [Python 3](https://www.python.org/downloads/)
- [The Stripe CLI](https://stripe.com/docs/payments/handling-payment-events#install-cli)

## Step by step

### 0. Start

Clone this repo. Open a terminal window and navigate to that folder.

### 1. Configure .env file

Go to the [API keys](https://dashboard.stripe.com/test/apikeys) page and copy the following values:
* *Publishable key* to the `STRIPE_PUBLISHABLE_KEY` value
* *Secret key* (click Reveal if necessary) to the `STRIPE_SECRET_KEY` value

Save the file.

### 2. Create and activate a new virtual environment

**MacOS / Unix**

```
python3 -m venv env
source env/bin/activate
```

**Windows (PowerShell)**

```
python3 -m venv env
.\env\Scripts\activate.bat
```

### 3. Install dependencies

```
pip install -r requirements.txt
```

### 4. Export and run the application

**MacOS / Unix**

```
export FLASK_APP=server/server.py
python3 -m flask run --port=4242
```

**Windows (PowerShell)**

```
$env:FLASK_APP=“server\server.py"
python3 -m flask run --port=4242
```

### 5. Start the stripe event hook

Open a new terminal in the same folder. If you haven't already, login to the Stripe CLI, replacing {{API_KEY}} with the `STRIPE_SECRET_KEY` from `.env`:

```
stripe login --api-key {{API_KEY}}
```

After that, start listening to the events from the server side

```
stripe listen --forward-to http://localhost:4242/webhook
```

### 6. Open the frontend

Go to [`localhost:4242/checkout`](http://localhost:4242/checkout) in your browser to see the demo.

# The experience

The experience is a simple payment form based on the accepting a card payment [sample](https://github.com/stripe-samples/accept-a-card-payment). 

The payment form is loaded at every page refresh. It can also be reloaded after a successful payment.

## Output

The server will generate and save an "order" each time the payment form is loaded (admittedly unrealistic) and will save "confirmed orders" to a separate file. The two files that stand for the database:
* `orders.txt` will log orders in a tab separated file with three columns: creation date, order id, order details
* `confirmed-orders.txt` will log each confirmed order in a tab separated file with two columns: confirmation date, order id

The files will be cleared each time the server is started.

## Test cases

The app should pass the [test cases](https://stripe.com/docs/payments/accept-a-payment#web-test-integration) described in the documentation.
